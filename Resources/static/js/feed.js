$(document).ready(function() {

  // Menu Action
  $("body").delegate(".dropdown-item", "click", function() {
    var feedId = $(this).attr("href");
    $.ajax({
      url: "getContent",
      type: "POST",
      data: { id: feedId, order: "DESC" }
    }).done(function(msg) {
      var htmlContent = msg;
      $("#rss-content").empty();
      $("#rss-content").append(htmlContent);
      reloadMenu(); // Removing until we change the UI  design the full menu is heavy
      return false;
    });
    return false;
  });

    function reloadMenu() {
        $.ajax({
            url: "getMenu",
            type: "POST",
        }).done(function(msg) {
            $(".left-menu ul").remove();
            $(".left-menu").append(msg);
            return false;
        });
    }

  // Keyboard Controll
  $(document).keypress(function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if ( code === 106 ) {
      var feedsContentId = $("input[name='unread']").first().parent().attr("id");
      if ( feedsContentId ) {
        $.ajax({
          url: "setRead",
          type: "POST",
          data: { feedsContentId: feedsContentId }
        }).done(function(msg) {
          $("input[name='unread']").first().parent().parent().slideToggle();
          $("input[name='unread']").first().attr("name", "read");
          // reloadMenu(); # Removing until we change the UI  design the full menu is heavy 
          return false;
        });
      }
    }
    if ( code === 107 ) {
      $("input[name='read']").last().parent().parent().slideToggle();
      $("input[name='read']").last().attr("name", "unread");
    }
  });

  // Add to favorites
  $("body").delegate("a.favorite", "click", function() {
    var link = $(this);
    var feedsContentId = $(this).attr("href");
    var feedId = $("input[name='feedId']").val();
    $.ajax({
      url: "saveFavorite",
      type: "POST",
      data: { id: feedsContentId, feedId: feedId }
    }).done(function(msg) {
      link.removeClass("-heart");
      link.addClass("-star");
      return false;
    });
    return false;
  });

  // Remove Favorite
  $("body").delegate("a.removeFavorite", "click", function() {
    var link = $(this);
    var feedsContentId = $(this).attr("href");
    var feedId = $("input[name='feedId']").val();
    $.ajax({
      url: "removeFavorite",
      type: "POST",
      data: { id: feedsContentId }
    }).done(function(msg) {
      var idtoremove = link.attr("href");
      $("#"+idtoremove).parent().hide();
      return false;
    });
    return false;
  });

  // Mark all posts from this feed Read
  $("body").delegate("a.markAllRead", "click", function() {
    $.ajax({
      url: "markAllRead",
      type: "POST",
      data: { userId: 1 }
    }).done(function() {
      location.reload();
      reloadMenu();
      return false;
    });
    return false;
  });

  return false;
})
