import feedparser as feed
import mysql.connector
import sys, time, datetime, os
import re
import hashlib
import string
from datetime import date, datetime
from datetime import timedelta
from time import strptime

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), "../")) + "/Controllers"
)
from configs import Configs

config = Configs()

con = mysql.connector.connect(
    user=os.environ.get("db_user"),
    password=os.environ.get("db_password"),
    host=os.environ.get("db_host"),
    database=os.environ.get("db_db"),
    buffered=True,
)


"""
Create Feed OBJ
"""


def create_feed_obj(url):
    return feed.parse(url)


"""
Get Http Status Code
"""


def get_feed_http_status(feed_obj):
    return "Status %s" % feed_obj["status"]


"""
Last Time it was updated
"""


def get_feed_last_updated(feed_obj):
    if "updated" in feed_obj["feed"]:
        return feed_obj["feed"]["updated"]


"""
Same has get_feed_last_updated but parsed
"""


def get_feed_last_updated_parsed(feed_obj):
    return feed_obj["feed"]["updated_parsed"]


"""
Encoding
"""


def get_feed_encoding(feed_obj):
    return feed_obj["encoding"]


"""
If well formated : Returns 0 if OK
"""


def get_feed_is_well_formatted(feed_obj):
    return feed_obj["bozo"]


"""
Href of the FEED
"""


def get_feed_url(feed_obj):
    return feed_obj["href"]


"""
RSS Feed Version
"""


def get_feed_version(feed_obj):
    return feed_obj["version"]


"""
Return RSS Entries
"""


def get_feed_entries(feed_obj):
    return feed_obj["entries"]


"""
Namespaces Used for the Feed
"""


def get_feed_namespaces(feed_obj):
    return feed_obj["namespaces"]


"""
Create Feed Attributes for Inserting into DB
"""


def get_attributes(feed_obj):
    link = ""
    title = ""
    summary = ""
    content = ""
    posted = "1970-01-01T00:00:00.000+01:00"

    if hasattr(feed_obj, "link"):
        link = feed_obj["link"]

    if hasattr(feed_obj, "title_detail"):
        title = feed_obj["title_detail"]["value"]

    if hasattr(feed_obj, "summary_detail"):
        summary = feed_obj["summary_detail"]["value"]

    if hasattr(feed_obj, "content"):
        content = feed_obj["content"][0]["value"]

    if hasattr(feed_obj, "published"):
        posted = feed_obj["published"]

    return [link, title, summary, content, posted]


def save_content(user_id, feed_id, url):
    feed = create_feed_obj(url)
    feed_entries = get_feed_entries(feed)
    if get_feed_http_status(feed) == "Status 404":
        update_status(feed_id)
        return False

    # For each entrie lets create the Query to insert into the db :
    # Link, Title, Summary ( Short Intro ), Content of the Feed, published date

    for feed_entry in feed_entries:
        feed_attributes = get_attributes(feed_entry)
        # Parse Dates Before Continue
        if re.search("\w{3},\s(\d{2})\s(\w{3})\s(\d{4})", feed_attributes[4]):
            posted_parse = re.search(
                "\w{3},\s(\d{2})\s(\w{3})\s(\d{4})\s(\d{2}\:\d{2}\:\d{2})",
                feed_attributes[4],
            )
            month = strptime(posted_parse.group(2), "%b").tm_mon
            feed_attributes[4] = (
                posted_parse.group(3)
                + "-"
                + str(month)
                + "-"
                + posted_parse.group(1)
                + " "
                + posted_parse.group(4)
            )
        article_hash = hashlib.md5()
        article_hash.update(feed_attributes[1].encode("utf-8").strip())
        article_hash.update(feed_attributes[3].encode("utf-8").strip())
        article_hash.update(feed_attributes[4].encode("utf-8").strip())
        crete_feed(feed_id, article_hash.hexdigest(), user_id, feed_attributes, 0)
    return True


"""
Method to control Date Field
"""
def check_time(str_time):
    try: 
        str_time = datetime.strptime(str_time, '%d %B %Y - %H:%M')
    except Exception as e:
        return str_time
    return str_time

"""
Method to insert into DB
"""
def crete_feed(feed_id, article_hash, user_id, feed_attributes, count):
    post_time = check_time(feed_attributes[4])
    if count == 3:
        return False
    try:
        insert = con.cursor()
        insert.execute(
            'INSERT INTO feeds_content (feedsId, article, userId, link, summary, title, content, unread, posted) VALUES (%s, %s, "%s", "%s", "%s", "%s", "%s", %s, %s)',
            (
                feed_id,
                article_hash,
                user_id,
                feed_attributes[0],
                feed_attributes[2],
                feed_attributes[1],
                feed_attributes[3],
                1,
                post_time,
            ),
        )
        print("New Article imported id %s" % feed_id)
        con.commit()
    except Exception as e:
        error = str(e)
        if error.find("Incorrect datetime value") != -1:
            feed_attributes[4] = date.now()
            crete_feed(feed_id, article_hash, user_id, feed_attributes, count + 1)
        if error.find("Incorrect string value") != -1:
            feed_attributes[2] = "-"
            feed_attributes[3] = "-"
            crete_feed(feed_id, article_hash, user_id, feed_attributes, count + 1)
        if error.find("Duplicate entry") != -1:
            return False
        print(error)
    return True


"""
Save Status of Importing
"""


def update_status(feed_id):
    feed = con.cursor()
    query = "SELECT errors FROM feeds WHERE id = %s" % feed_id
    feed.execute(query)

    for f in feed:
        if f[0] >= 5:
            return False
        else:
            error_count = f[0] + 1
            query = "UPDATE feeds SET errors = %s where id = %s" % (
                error_count,
                feed_id,
            )
            feed.execute(query)
            con.commit()
            return True


"""
Get Status of Importing
"""


def get_status(feed_id):
    feed = con.cursor()
    query = "SELECT errors FROM feeds WHERE id = %s" % feed_id
    feed.execute(query)

    for f in feed:
        if f[0] >= 5:
            return False
    else:
        return True


def get_users():
    print("Getting Users")
    users = con.cursor()
    users.execute("SELECT id FROM users")

    for user in users:
        print("User: %s" % user[0])
        user_id = user[0]
        query = "SELECT * FROM feeds WHERE userId = %s and disabled is null" % user_id
        feeds = con.cursor()
        feeds.execute(query)
        for feed in feeds:
            print("Getting feed: %s" % feed[4])
            url = feed[2]
            feed_date = feed[5]

            # Import only if we did not do that on the last 5 minutes
            date = datetime.now() - timedelta(minutes=int(5))
            update = con.cursor()
            # save_content(user_id, feed[0], url)
            try:
                if date > feed_date or date is None or 1:
                    feed_id = feed[0]
                    try:
                        save_content(user_id, feed_id, url)
                    except Exception as e:
                        #update_status(feed_id)
                        print("[Error] There was an error - %s %s" % (e, feed_id))
                    finally:
                        query = (
                            "UPDATE feeds SET last_check = NOW() WHERE id = %s"
                            % feed_id
                        )
                        update.execute(query)
                        con.commit()
            except Exception as e:
                print("[Error] THere was a error Daisy %s" % url)
                print("[Error]  was %s" % e)

    return True


get_users()
