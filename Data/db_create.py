import mysql.connector
import xml.etree.ElementTree as ET
import os, sys

# Necessary files
# default_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), "../")) + "/Controllers"
)

# Database Config file loaded :)
from configs import Configs

config = Configs()

con = mysql.connector.connect(
    user=os.environ.get("db_user"),
    password=os.environ.get("db_password"),
    host=os.environ.get("db_host"),
    database=os.environ.get("db_db"),
)
cur = con.cursor()

print("Drop old Tables")
cur.execute("DROP TABLE IF EXISTS users")
cur.execute("DROP TABLE IF EXISTS feeds")
cur.execute("DROP TABLE IF EXISTS feeds_content")
cur.execute("DROP TABLE IF EXISTS feeds_favorites")

print("Creatinf new Tables")
cur.execute(
    "CREATE TABLE users (id INTEGER PRIMARY KEY AUTO_INCREMENT, name TEXT, pass TEXT)"
)
cur.execute(
    "CREATE TABLE feeds (id INTEGER PRIMARY KEY AUTO_INCREMENT, userId INTEGER, url TEXT, siteUrl TEXT, name TEXT, last_check DATETIME, errors INTEGER)"
)
cur.execute(
    "CREATE TABLE feeds_content (id INTEGER PRIMARY KEY AUTO_INCREMENT, article VARCHAR(250) UNIQUE, feedsId INTEGER, userId INTEGER, link TEXT, summary TEXT, title TEXT, content TEXT, unread INTEGER, posted DATETIME, readAT DATE DEFAULT NULL)"
)
cur.execute(
    "CREATE TABLE feeds_favorites ( id INTEGER PRIMARY KEY AUTO_INCREMENT, feedsId INTEGER, feedsContentId INTEGER, userId INTEGER)"
)

# TODO REMOVE THIS FROM HERE
cur.execute("INSERT INTO users (name,pass) VALUES ('ammartins', 'testtest')")

con.commit()
con.close()
