import mysql.connector
import xml.etree.ElementTree as ET
import sys, os

# Necessary files
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../')) + '/Controllers')

# Database Config file loaded :)
from configs import Configs
config = Configs()

con = mysql.connector.connect(
    user=os.environ.get("db_user"),
    password=os.environ.get("db_password"),
    host=os.environ.get("db_host"),
    database=os.environ.get("db_db"),
)
cur = con.cursor()

print("Updating Database")

try:
    cur.execute("ALTER TABLE feeds ADD disabled int")
except Exception as e:
    print(e)
    print("Skipping Update 1")
finally:
    print("Disabled Field Created")

try:
    cur.execute("ALTER TABLE `feeds_content` CHANGE `readAT` `readAT` DATETIME NULL DEFAULT NULL")
except Exception as e:
    print(e)
    print("Skipping Update 2")
finally:
    print("readAT Field Changes")

try:
    cur.execute("ALTER TABLE feeds_content MODIFY COLUMN article VARCHAR(250)")
except Exception as e:
    print(e)
    print("Skipping Update 3")
finally:
    print("article Field alterd")

try:
    cur.execute('ALTER TABLE feeds RENAME COLUMN url TO feed_url');
except Exception as e:
    print(e)
    print("Skipping Update 4")
finally:
    print("Url column reanme feed_url")