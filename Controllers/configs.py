import os
import configparser


class Configs:
    default_path = os.path.dirname(os.path.realpath(__file__))
    config = configparser.ConfigParser()
    # config.read(os.path.normpath(os.path.join(default_path, "../Data/Database.yml")))

    def __init__(self):
        # Get Config
        self.user = os.environ.get("db_user")
        self.host = os.environ.get("db_host")
        self.password = os.environ.get("db_password")
        self.database_name = os.environ.get("db_db")
