import os, sys, json, bottle
import re

from bottle import route, run, static_file, error, TEMPLATE_PATH, post, get, request
from bottle import jinja2_template as template
from wtforms import Form, BooleanField, StringField, validators

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), ".")) + "/Controllers"
)
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), ".")) + "/Models"
)

from Feeds import Feeds

TEMPLATE_PATH.append("Resources/templates")

# Static Files
@route("/Resources/static/<path:path>")
def server_static(path):
    return static_file(path, root="Resources/static/")

@route("/reader")
def reader(userId=1):
    feeds_obj = Feeds()
    feed_count = feeds_obj.getTotal()   
    feeds = feeds_obj.getFeedsForMenu(userId)
    menu = True
    graphData = feeds_obj.getStats("monthPosts")
    dataMonth = json.dumps(graphData)
    graphData = feeds_obj.getStats("dayPosts")
    dataDay = json.dumps(graphData)
    return template(
        "reader.html",
        feeds_url=feeds,
        feed_count=feed_count,
        manu=menu,
        dataMonth=dataMonth,
        dataDay=dataDay,
    )

@route("/status")
def status(userId=1):
    feeds_obj = Feeds()
    stats = feeds_obj.getStatsReads(userId)
    feeds = feeds_obj.getErroFeeds(userId)
    return template("status.html", feeds=feeds, stats=stats)

# Get Favorite
@route("/favorites")
def getFavorite():
    feeds_obj      = Feeds()
    feeds_favorite = feeds_obj.getFavorite(1)
    feed_count = 10
    return template(
        "show_favoritos.html", articles=feeds_favorite, feed_count=feed_count
    )

# Search Page
@route("/search")
def search():
    feeds_obj = Feeds()
    searchQuery = request.query["search"]
    result = feeds_obj.search(searchQuery, 1)
    menu = True
    feed_count = feeds_obj.getTotal()
    return template(
        "show_favoritos.html", articles=result, menu=menu, feed_count=feed_count
    )

# Mark Current Feed as Read
@post("/readAll")
def readAll():
    feeds_obj = Feeds()
    feedId = request.POST.id
    feeds_obj.readAll(feedId)

# Mark a Feed Disabled
@post("/disable")
def disable():
    feeds_obj = Feeds()
    feedId = request.POST.id
    feeds_obj.disable(feedId)

@post("/markAllRead")
def markAllRead(userId=1):
    feeds_obj = Feeds()
    userId = 1
    feeds_obj.markAllRead(userId)

# Get Feed Cntent
@post("/getContent")
def getContent():
    feeds_obj = Feeds()
    feedId = request.POST.id
    userId = 1
    unread = 1
    order = request.POST.order
    articles = feeds_obj.getFeedsContent(userId, feedId, "all", unread, order)
    for article in articles:
        article = list(article)
        article[3] = re.sub('(\<img .*\/\>)', "", article[3])
    menu = True
    return template("show_feeds.html", articles=articles, feedId=feedId, menu=menu)

# Mark as Read
@post("/setRead")
def setRead():
    feeds_obj = Feeds()
    feedId = request.POST.feedsContentId
    userId = 1
    feeds_obj.setRead(feedId, userId)
    return []

# Save to Favorites
@post("/saveFavorite")
def saveFavorite():
    feeds_obj = Feeds()
    postId = request.POST.id
    feedId = request.POST.feedId
    userId = 1
    feeds_obj.saveFavorite(postId, userId, feedId)

# Save to Favorites
@post("/removeFavorite")
def removeFavorite():
    feeds_obj = Feeds()
    postId = request.POST.id
    feeds_obj.removeFavorite(postId)

# Reload Menu
@post("/getMenu")
def getMenu():
    feeds_obj = Feeds()
    userId = 1
    feed_count = feeds_obj.getTotal()
    feeds = feeds_obj.getFeedsForMenu(userId)
    return template("show_menu.html", feeds_url=feeds, feed_count=feed_count)

# Add new Feed
@route("/newFeed", method=["GET", "POST"])
def newFeed():
    feeds_obj = Feeds()
    form = FeedForm(request.POST)
    if request.method == "POST":
        feeds_obj.newFeed(form.url.data, form.rss.data, form.name.data)
    return template("new_feed.html", form=form)

class FeedForm(Form):
    url  = StringField("Site Url", [validators.Length(min=4, max=250)])
    rss  = StringField("Rss Feed", [validators.Length(min=4, max=250)])
    name = StringField("Site Name", [validators.Length(min=4, max=25)])


# Setting Debug Mode
# Starting the SERVER
application = bottle.default_app()
run(host="0.0.0.0", port=8090, debug=True, reloader=True)
