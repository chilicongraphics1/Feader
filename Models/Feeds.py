import mysql.connector
import os, sys
from configs import Configs


class Feeds:
    def __init__(self):
        self.config = Configs()
        # Remove this config from here and create a config FILE
        self.con = mysql.connector.connect(
            user=os.environ.get("db_user"),
            password=os.environ.get("db_password"),
            host=os.environ.get("db_host"),
            database=os.environ.get("db_db"),
            buffered=True,
        )
        self.cur = self.con.cursor()
        self.array_list = []

    def newFeed(self, feed, url, name):
        self.cur.execute(
            'INSERT INTO feeds (userId, feed_url, siteUrl, name, last_check, errors) VALUES (%s, "%s", "%s", "%s", NOW(), 0)'
            % (1, url, feed, name)
        )
        self.con.commit()

    def search(self, searchQuery, userId):
        order = "DESC"
        self.cur.execute(
            "SELECT link, title, content, summary, id, unread FROM feeds_content WHERE userId = %s and title like '%s' ORDER BY id %s"
            % (userId, "%" + searchQuery + "%", order)
        )
        ret = self.cur.fetchall()
        return ret

    def getFeeds(self, userId, ammount):
        if ammount == "all":
            return self.cur.execute("SELECT * FROM feeds WHERE userId = %s" % userId)
        elif ammount == "single":
            return self.cur.execute(
                "SELECT * FROM feeds WHERE userId = %s" % userId
            ).fetchone()
        else:
            return self.cur.execute("SELECT * FROM feeds WHERE userId = %s" % userId)

    def getFeedsContent(self, userId, feedsId, ammount, unread, order):
        if ammount == "all":
            self.cur.execute(
                "SELECT link, title, content, summary, id, unread, posted FROM feeds_content WHERE userId = %s and feedsId = %s and unread = %s ORDER BY id %s"
                % (userId, feedsId, unread, order)
            )
        elif ammount == "single":
            self.cur.execute(
                "SELECT link, title, content, summary, id, unread, posted FROM feeds_content WHERE userId = %s and feedsId = %s and unread = %s ORDER BY id %s"
                % (userId, feedsId, unread, order)
            ).fetchone()
        else:
            self.cur.execute(
                "SELECT link, title, content, summary, id, unread, posted FROM feeds_content WHERE userId = %s and feedsId = %s and unread = %s ORDER BY id %s"
                % (userId, feedsId, unread, order)
            )
        ret = self.cur.fetchall()
        return ret

    def getFeedsContentCounter(self, userId):
        return self.cur.execute(
            "select feedsId, count(*) from feeds_content where userId = 1 group by feedsId order by feedsId;"
        )

    def getTotal(self):
        self.cur.execute(
            "SELECT count(*) FROM feeds_content WHERE userId = 1 and unread = 1"
        )
        for teste in self.cur:
            return teste

    def getFeedsForMenu(self, userId):
        self.cur.execute(
            "select b.siteUrl,  b.feed_url, b.name, count(*), b.id from feeds_content as a INNER JOIN feeds as b where b.id = a.feedsId and a.userId = %s and unread = 1 group by feedsId order by b.name"
            % userId
        )
        ret = self.cur.fetchall()
        return ret

    def getErroFeeds(self, userId):
        self.cur.execute("SELECT * FROM feeds WHERE errors > 0")
        ret = self.cur.fetchall()
        return ret

    def getStatsReads(self, userId):
        self.cur.execute(
            "SELECT f.name, fc.unread as status, count(fc.id) as numberOf FROM feeds_content as fc JOIN feeds as f ON f.id = fc.feedsId GROUP BY fc.feedsId, fc.unread Order By f.name"
        )
        ret = self.cur.fetchall()
        return ret

    def setRead(self, feedsContentId, userId):
        self.cur.execute(
            "UPDATE feeds_content SET readAT = NOW() WHERE id = %s and unread = 1"
            % feedsContentId
        )
        self.cur.execute(
            "UPDATE feeds_content SET unread = 0 WHERE id = %s and unread = 1"
            % feedsContentId
        )
        self.con.commit()

    def disable(self, feedId):
        self.cur.execute("UPDATE feeds SET disabled = 1 WHERE id = %s" % feedId)
        self.con.commit()
        return True

    def readAll(self, feedId):
        self.cur.execute(
            "UPDATE feeds_content SET readAT = NOW() WHERE feedsId = %s and unread = 1"
            % feedId
        )
        self.cur.execute(
            "UPDATE feeds_content SET unread = 2 WHERE feedsId = %s and unread = 1"
            % feedId
        )
        self.con.commit()
        return True

    def getFavorite(self, userId):
        order = "DESC"
        self.cur.execute(
            "SELECT link, title, content, summary, id, unread FROM feeds_content WHERE userId = %s and id in (SELECT feedsContentID FROM feeds_favorites WHERE userId = %s) ORDER BY id %s"
            % (userId, userId, order)
        )
        ret = self.cur.fetchall()
        return ret

    def saveFavorite(self, feedsContentId, userId, feedId):
        self.cur.execute(
            "INSERT INTO feeds_favorites (feedsId, feedsContentId, userId) VALUES (%s, %s, %s)"
            % (feedId, feedsContentId, userId)
        )
        self.con.commit()

    def newForm(self, name, siteUrl, feedUrl):
        sys.exit(1)
        self.cur.execute(
            "INSERT INTO feeds (userid, url, siteUrl, name) VALUES ('%s', '%s', '%s', '%s')"
            % (1, feedUrl, siteUrl, name)
        )
        self.con.commit()

    def removeFavorite(self, feedsContentId):
        self.cur.execute(
            "DELETE FROM feeds_favorites WHERE feedsContentId = %s" % feedsContentId
        )
        self.con.commit()

    def markAllRead(self, userid):
        self.cur.execute(
            "UPDATE feeds_content SET readAT = NOW() WHERE userID = %s and unread != 2"
            % userid
        )
        self.cur.execute(
            "UPDATE feeds_content SET unread = 3 WHERE userID = %s and unread = 1"
            % userid
        )
        self.con.commit()

    def getStats(self, stat="topPoster"):
        if stat == "topPoster":
            self.cur.execute(
                "select b.name, count(*) as counter from feeds_content as a inner join feeds as b on a.feedsId = b.id GROUP BY feedsId ORDER BY counter DESC limit 10"
            )
        elif stat == "monthPosts":
            self.cur.execute(
                "SELECT MONTH(posted), count(*) FROM feeds_content WHERE YEAR(posted) = YEAR(curtime()) group by MONTH(posted)"
            )
        elif stat == "dayPosts":
            self.cur.execute(
                "SELECT DAY(posted), count(*) FROM feeds_content WHERE YEAR(posted) = YEAR(curtime()) and MONTH(posted) = MONTH(curtime()) group by DAY(posted)"
            )
        self.toArray()
        return self.array_list

    def getAllUnread(self, userId=1):
        return self.cur.execute(
            "SELECT link, title, content, summary, id, unread, posted FROM feeds_content WHERE userId = 1 and unread = 0"
        )

    def toArray(self):
        for item in self.cur:
            self.array_list.append(item[1])
