FROM python:3.10.0-slim as builder

RUN apt-get update

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get install gcc software-properties-common -y

COPY ./requirments.txt /srv/

WORKDIR /srv

RUN pip3 install --user -r requirments.txt

RUN apt-get purge gcc software-properties-common -y && \
    apt-get autoclean -y

FROM python:3.10.0-slim

COPY --from=builder /root/.local /root/.local

ADD Actions/feed_cron.py /srv/Actions/feed_cron.py
ADD Data/db_updates.py /srv/update/db_updates.py
ADD Controllers/configs.py /srv/Controllers/configs.py
ADD requirments.txt /srv/requirments.txt

WORKDIR /srv

ENTRYPOINT python3 update/db_updates.py ; python3 Actions/feed_cron.py